import template from './extension/sw-product-settings-form/sw-product-settings-form.html.twig';

const Criteria = Shopware.Data.Criteria;

Shopware.Component.override('sw-product-settings-form', {
    template,
    methods: {
        getProductVariantCriteria() {
            const criteria = new Criteria();
            criteria.filters = [
                {
                    type: 'equals',
                    field: 'parentId',
                    value: this.product?.id ?? -1
                }
            ]
            criteria.addSorting(Criteria.sort('name', 'ASC', true));
            return criteria;
        },
        getProductVariantContext(){
            return Shopware.Context.api;
        },
        getProductVariantEntity() {
            return 'product';
        }
    }
});